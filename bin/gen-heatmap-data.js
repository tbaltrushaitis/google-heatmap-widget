/*!
 * Project: thingsboard.io
 * File:    ./gen-heatmap-data.js
 * Author:  Baltrushaitis Tomas
 * Created: 2017-12-19
 */

'use strict';

/*
 * DEPENDENCIES
 * @private
 */

const fs = require('fs');
const path = require('path');
const util = require('util');

// Load Chance
var Chance = require('chance');

/*
 * CONFIGURATION
 */

// Instantiate Chance so it can be used
const ch = new Chance();

const cntPlots = 500;
const minWeight = 1;
const maxWeight = 20;
const fOut = 'resources/telemetry-data.json';

util.inspect.defaultOptions = {
  showHidden: false,
  depth: 4,
  colors: true
};

let aPlots = [];

for (let i = 0; i < cntPlots; i++) {
  var crdLat = ch.latitude();
  var crdLng = ch.longitude();
  var coords = ch.coordinates();
  var d = ch.natural({min: minWeight, max: maxWeight});
  var cData = {latitude: crdLat, longitude: crdLng, rssi: d};
  aPlots.push(cData);
  console.log(`${i}: [${util.inspect(cData)}]`);
}

fs.writeFileSync(fOut, JSON.stringify(aPlots));
console.info(`Generated file [${fOut}] with [${cntPlots}] points!`);
