#!/usr/bin/env bash

DIR_DATA=resources

# Set ThingsBoard host to "demo.thingsboard.io" or "localhost"
export THINGSBOARD_HOST=demo.thingsboard.io

# Replace YOUR_ACCESS_TOKEN with one from Device credentials window.
export ACCESS_TOKEN=AV5eTxKtOH0fP0uXcFYE

# Read serial number and firmware version attributes
ATTRIBUTES=$(cat ${DIR_DATA}/attributes-data.json )
export ATTRIBUTES

# Read timeseries data as an object without timestamp (server-side timestamp will be used)
TELEMETRY=$(cat ${DIR_DATA}/telemetry-data.json)
export TELEMETRY

# publish attributes and telemetry data via mqtt client
node ./publish.js
