var map, mapOptions, mapData, mapFlex, mapCanvas, mapCenter
  , latCenter, lngCenter
  , Gradients
  , heatmap, heatMapOptions, heatMapData
;

window.onInit = self.onInit = function() {
  console.log('onInit()');

  mapData = [];
  heatMapData = [];

  latCenter = 50.433969;
  lngCenter = 50.52178;

  Gradients = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
  ];

//   initMap();
}

self.onDataUpdated = function() {
  var flex = {
      latitude:  self.ctx.data[0].data.shift()[1]
    , longitude: self.ctx.data[1].data.shift()[1]
    , rssi:      self.ctx.data[2].data.shift()[1]
  };
  updateHeatmapData(flex);
}
//   console.log('DataUpdate with:', flex);

self.onResize = function() {
  // self.ctx.flot.resize();
}

self.onEditModeChanged = function() {
  // self.ctx.flot.checkMouseEvents();
}

self.onMobileModeChanged = function() {
  // self.ctx.flot.checkMouseEvents();
}

self.getSettingsSchema = function() {
  // return TbFlot.settingsSchema;
}

self.getDataKeySettingsSchema = function() {
  // return TbFlot.datakeySettingsSchema(true);
}

self.onDestroy = function() {
  // self.ctx.flot.destroy();
}

// Map Initialization
window.initMap = function () {
  console.log('initMap()');

  mapCenter = new google.maps.LatLng(latCenter, lngCenter);
  mapCanvas = document.getElementById("mapCanvas");

  mapOptions = {
      center: mapCenter
    , mapTypeId: google.maps.MapTypeId.SATELLITE
    , zoom: 1
    , maxZoom: 12
  };
  map = new google.maps.Map(mapCanvas, mapOptions);

  new google.maps.Circle({
    map:            map,
    center:         mapCenter,
    radius:         500000,
    strokeColor:    "#0000FF",
    strokeOpacity:  0.8,
    strokeWeight:   2,
    fillColor:      "#0000FF",
    fillOpacity:    0.3
  });
  new google.maps.Circle({
    map:            map,
    center:         new google.maps.LatLng(latCenter + 20, lngCenter - 55),
    radius:         500000,
    strokeColor:    "#CFB095",
    strokeOpacity:  0.8,
    strokeWeight:   2,
    fillColor:      "#CFB095",
    fillOpacity:    0.3
  });
  new google.maps.Circle({
    map:            map,
    center:         new google.maps.LatLng(latCenter + 20, lngCenter + 55),
    radius:         500000,
    strokeColor:    "#99ff99",
    strokeOpacity:  0.8,
    strokeWeight:   2,
    fillColor:      "#99ff99",
    fillOpacity:    0.3
  });

  heatMapOptions = {
      gradient: Gradients
    , data:     heatMapData
    , dissipating: true
    , opacity:  0.9
  };
  heatmap = new google.maps.visualization.HeatmapLayer(heatMapOptions);

  deploySampleData();
  toggleHeatmap();
}

window.updateHeatmapData = self.updateHeatmapData = function (v) {
  let coords = new google.maps.LatLng(v.latitude, v.longitude);
  let p = {
      location: coords
    , weight:   v.rssi
  };
  heatMapData.push(p);
  heatmap.setData(heatMapData);
  // console.log('updateHeatmapData with [', p, ']');
}

window.deploySampleData = function () {
  console.log('deploySampleData()');

  var sampleData = [
      {latitude: -13.46111, longitude: -20.81837,  rssi: 71}
    , {latitude: 13.43587,  longitude: -59.2127,   rssi: 31}
    , {latitude: -72.40919, longitude: 150.69204,  rssi: 100}
    , {latitude: -47.57039, longitude: 16.5429,    rssi: 42}
    , {latitude: -42.20016, longitude: -79.05033,  rssi: 21}
    , {latitude: -88.84015, longitude: 42.82577,   rssi: 89}
    , {latitude: -24.30414, longitude: 2.82312,    rssi: 24}
    , {latitude: -0.47865,  longitude: -110.62196, rssi: 14}
    , {latitude: -21.73189, longitude: -160.07985, rssi: 39}
    , {latitude: 16.19713,  longitude: 20.97191,   rssi: 42}
  ];

  sampleData.forEach(function (lo, i) {
    updateHeatmapData(lo);
  });

}

window.toggleHeatmap = self.toggleHeatmap = function () {
  heatmap.setMap(heatmap.getMap() ? null : map);
  console.log('toggledHeatmap');
}

window.changeGradient = function () {
  heatmap.set('gradient', heatmap.get('gradient') ? null : Gradients);
  console.log('changedGradient');
}

window.changeRadius = function () {
  heatmap.set('radius', heatmap.get('radius') ? null : 20);
  console.log('changeRadius() to [', heatmap.get('radius'), ']');
}

window.changeOpacity = function () {
  heatmap.set('opacity', heatmap.get('opacity') ? null : 0.5);
  console.log('changedOpacity');
}

window.changeMaximum = function () {
  heatmap.set('maxIntensity', heatmap.get('maxIntensity') ? null : 90);
  console.log('changedMaximum');
}

window.wsConn = function () {
  var token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzYXhhenpvLm1hcnRpbmVsbGlAeWFob28uY29tIiwic2NvcGVzIjpbIlRFTkFOVF9BRE1JTiJdLCJ1c2VySWQiOiIxMWJiZjI1MC1lMjE1LTExZTctYjg2My0xZDhkMmVkZjRmOTMiLCJmaXJzdE5hbWUiOiJTYXhhenpvIiwibGFzdE5hbWUiOiJNYXJ0aW5lbGxpIiwiZW5hYmxlZCI6dHJ1ZSwiaXNQdWJsaWMiOmZhbHNlLCJ0ZW5hbnRJZCI6IjExYmJhNDMwLWUyMTUtMTFlNy1iODYzLTFkOGQyZWRmNGY5MyIsImN1c3RvbWVySWQiOiIxMzgxNDAwMC0xZGQyLTExYjItODA4MC04MDgwODA4MDgwODAiLCJpc3MiOiJ0aGluZ3Nib2FyZC5pbyIsImlhdCI6MTUxNTY2NjU0MSwiZXhwIjoxNTI0NjY2NTQxfQ.EdvDyhd7gx1f1qOOYebi7r4pd7T4EfSMj9Vl-252KB7cutd2t9pTjpULUEGyQN7RSSR7IatOuvnVyVlJzfCDUg";
  // var entityId = "27bbfd60-e469-11e7-b863-1d8d2edf4f93";
  var entityId = "271fa750-f736-11e7-abe9-1d8d2edf4f93";
  var WS = new WebSocket("wss://demo.thingsboard.io/api/ws/plugins/telemetry?token=" + token);

  if ('' === entityId) {
    console.log("Invalid device id!");
    WS.close();
  }

  if ('' === token) {
    console.log("Invalid JWT token!");
    WS.close();
  }

  WS.onopen = function () {
    console.log('webSocket Connection is OPENED!');
    var lo = {
      tsSubCmds: [
        {
            entityType: 'DEVICE'
          , entityId:   entityId
          , scope:      'LATEST_TELEMETRY'
          , cmdId:      10
        }
      ],
      historyCmds: [],
      attrSubCmds: []
    };
    var data = JSON.stringify(lo);
    WS.send(data);
    console.log('Sent MSG:', data);
  };

  WS.onmessage = function (evt) {
    var msg = JSON.parse(evt.data);
    var flex = {
        latitude:   msg.data.latitude[0][1]
      , longitude:  msg.data.longitude[0][1]
      , rssi:       parseInt(msg.data.rssi[0][1])
    };
    updateHeatmapData(flex);
  };

  WS.onclose = function (evt) {
    console.log('webSocket Connection is CLOSED!');
  }

}
