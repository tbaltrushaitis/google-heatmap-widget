/*!
 * Project: thingsboard.io
 * File:    ./src/google-heatmap.js
 * Author:  Baltrushaitis Tomas
 * Created: 2017-12-19
 */

'use strict';

var map, heatmap, crdCenter, mapOptions, heatMapOptions, heatMapGradient, heatMapData;

function initMap() {

  crdCenter = new google.maps.LatLng(37.775, -122.434);

  mapOptions = {
    zoom: 5,
    center: crdCenter,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  };

  heatMapGradient = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
  ];

  heatMapData = [
    {location: new google.maps.LatLng(37.782, -122.447), weight: 0.5},
    {location: new google.maps.LatLng(37.782, -122.445), weight: 1},
    {location: new google.maps.LatLng(37.782, -122.443), weight: 2},
    {location: new google.maps.LatLng(37.782, -122.441), weight: 3},
    {location: new google.maps.LatLng(37.782, -122.439), weight: 2},
    {location: new google.maps.LatLng(37.782, -122.437), weight: 1},
    {location: new google.maps.LatLng(37.782, -122.435), weight: 0.5},

    {location: new google.maps.LatLng(37.785, -122.447), weight: 3},
    {location: new google.maps.LatLng(37.785, -122.445), weight: 2},
    {location: new google.maps.LatLng(37.785, -122.443), weight: 1},
    {location: new google.maps.LatLng(37.785, -122.441), weight: 0.5},
    {location: new google.maps.LatLng(37.785, -122.439), weight: 1},
    {location: new google.maps.LatLng(37.785, -122.437), weight: 2},
    {location: new google.maps.LatLng(37.785, -122.435), weight: 3}
  ];

  heatMapOptions = {
    map: map,
    data: heatMapData,
    dissipating: true,
    gradient: heatMapGradient,
    maxIntensity: 3,
    radius: 10,
    opacity: 0.9
  };

  map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);
  heatmap = new google.maps.visualization.HeatmapLayer(heatMapOptions);
}

function toggleHeatmap() {
  heatmap.setMap(heatmap.getMap() ? null : map);
}

function changeGradient() {
  heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
}

function changeRadius() {
  heatmap.set('radius', heatmap.get('radius') ? null : 20);
}

function changeOpacity() {
  heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
}
